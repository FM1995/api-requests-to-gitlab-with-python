# Python: API Request to GitLab

#### Project Outline

In this project we will see how can use Python to talk to external applications, in this project we will use GitLab as the external application
We will use Python to make HTTP requests and then get a HTTP response from GitLab, therefore utilise API requests


#### Lets get started

We will proceed in utilising a python module called requests

![Image 1](https://gitlab.com/FM1995/api-requests-to-gitlab-with-python/-/raw/main/Images/Image1.png)


```
pip install requests
```

And can see the download is successful

![Image 2](https://gitlab.com/FM1995/api-requests-to-gitlab-with-python/-/raw/main/Images/Image2.png)

Can see the requests package in the list

![Image 3](https://gitlab.com/FM1995/api-requests-to-gitlab-with-python/-/raw/main/Images/Image3.png)

Now lets implement the functionality using the below links

https://docs.gitlab.com/ee/api/rest/

https://docs.gitlab.com/ee/api/projects

![Image 4](https://gitlab.com/FM1995/api-requests-to-gitlab-with-python/-/raw/main/Images/Image4.png)

And then have the base URL

![Image 5](https://gitlab.com/FM1995/api-requests-to-gitlab-with-python/-/raw/main/Images/Image5.png)

And based on the above documentation can then configure the get request to the below

![Image 6](https://gitlab.com/FM1995/api-requests-to-gitlab-with-python/-/raw/main/Images/Image6.png)

And then configure it like the below with my gitlab id

![Image 7](https://gitlab.com/FM1995/api-requests-to-gitlab-with-python/-/raw/main/Images/Image7.png)

Can then modify to put the response from the API in the ‘response’ variable, then parse the response as JSON and store it in the my_projects variable. Can then iterate through each project using a loop. Inside the loop, it prints information about each project, specifically the project name and project URL aswell as using ‘f’ so that the values get correctly placed

Can now run it and see it as a success

![Image 8](https://gitlab.com/FM1995/api-requests-to-gitlab-with-python/-/raw/main/Images/Image8.png)


